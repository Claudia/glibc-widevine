commit 26d01172f5c3f3b912ecf21ebb911eb5351bba4d
Author: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date:   Fri Jan 26 08:23:41 2024 -0300

    misc: tst-poll: Proper synchronize with child before sending the signal
    
    When running the testsuite in parallel, for instance running make -j
    $(nproc) check, occasionally tst-epoll fails with a timeout.  It happens
    because it sometimes takes a bit more than 10ms for the process to get
    cloned and blocked by the syscall.  In that case the signal is
    sent to early, and the test fails with a timeout.
    
    Checked on x86_64-linux-gnu.

diff --git a/sysdeps/unix/sysv/linux/tst-epoll.c b/sysdeps/unix/sysv/linux/tst-epoll.c
index 3b38beae6e..545f506faa 100644
--- a/sysdeps/unix/sysv/linux/tst-epoll.c
+++ b/sysdeps/unix/sysv/linux/tst-epoll.c
@@ -22,7 +22,7 @@
 #include <support/support.h>
 #include <support/xsignal.h>
 #include <support/xunistd.h>
-#include <support/xtime.h>
+#include <support/process_state.h>
 #include <stdlib.h>
 #include <sys/epoll.h>
 
@@ -97,8 +97,8 @@ test_epoll_basic (epoll_wait_check_t epoll_wait_check)
   xclose (fds[0][0]);
   xclose (fds[1][1]);
 
-  /* Wait some time so child is blocked on the syscall.  */
-  nanosleep (&(struct timespec) {0, 10000000}, NULL);
+  /* Wait until child is blocked on epoll_wait.  */
+  support_process_state_wait (p, support_process_state_sleeping);
   TEST_COMPARE (kill (p, SIGUSR1), 0);
 
   int e = epoll_wait_check (efd, &event, 1, 500000000, &ss);
