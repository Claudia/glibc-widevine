glibc Debian source package
---------------------------

This package source repository is organized by the following set of
rules, tags, and branches:

Rules
~~~~~

1. Each branch corresponds to the Debian package source for a specific
   upstream major/minor release.

2. Once upstream releases a stable major/minor version, its tarball
   does not change for the lifetime of that branch.

3. Any maintenance updates from upstream are kept in
   `debian/patches/git-updates.diff` as part of the patch series,
   separately from the original upstream tarball.

Branches
~~~~~~~~

- `sid`

  Debian packaging source for the major/minor release associated with
  the `sid` distribution.

  The version selected by the Debian maintainers corresponds to, or
  is older than, the latest stable upstream release’s major/minor
  version.

- `glibc-X.Y`

  Debian packaging source for the upstream major/minor release `X.Y`.

  This branch is typically created from an older branch following
  the release of upstream’s X.Y source.
  Migrating to a new source tarball involves reapplying the patch series
  to align with the tarball and adding a new entry to `debian/changelog`
  with the version number `X.Y-0experimental0`.

  As the branch matures, it may produce several `X.Y-0experimentalZ`
  package releases until it is ready for the `X.Y-1` release.
  From this point, the branch might be merged into `sid` immediately
  or at a later time. If delayed, it might receive one or more `X.Y-Z`
  releases before merging. After merging with `sid`, this branch is
  typically no longer updated.

- `CODENAME`, `CODENAME-security`

  Maintenance branches for a specific Debian package release associated
  with the `CODENAME` Debian distribution.

  For example, if glibc version 2.36-9 is released with Bookworm, the
  `bookworm` branch is created from the commit tagged `debian/2.36-9`.
  Subsequent Debian packages, such as versions 2.36-9+deb12u1 and
  2.36-9+deb12u2, are released from this branch.

Tags
~~~~

- `debian/X.Y-Z`

  Debian package release.

  This tag can originate from either a `glibc-X.Y` branch or the `sid`
  branch.

- `debian/X.Y-0experimentalZ`

  Debian package that is not yet considered stable enough for
  inclusion in `sid`. This tag is usually applied to a revision on a
  `glibc-X.Y` branch.

- `debian/X.Y-Z+debAAuBB`

  Debian package update for a specific distribution.

  This tag usually applies to revisions on the `CODENAME` or
  `CODENAME-security` branches.

  The `AA` part in the tag denotes the major version number of the
  Debian distribution associated with `CODENAME`.

  `BB` represents the update number within that distribution.

  The `X.Y-Z` portion remains unchanged across the distribution.
